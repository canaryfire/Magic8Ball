import random
from random import randint
from time import sleep

print('\n' * 100)
yesorno = "yes"
while yesorno == "yes":
    ques = input("This is the magic 8Ball. Please enter your question.")

    # set seed and gen number
    random.seed(a=ques, version=2)
    random.randrange(1, 3)
    randomint = randint(0, 3)

    # screenclear
    print('\n' * 100)

    print("Thinking...")

    # pause
    sleep(2)

    print('\n' * 100)

    # print statements
    if randomint == 0:
        print("It is certain.")
    elif randomint == 1:
        print("It is uncertain.")
    elif randomint == 2:
        print("It is likely.")
    elif randomint == 3:
        print("It is unlikely.")

    # choice
    sleep(2)
    yesorno = input("Do you want to try again? (yes/no)")
    yesorno.lower()
    if yesorno == "no":
        # quit
        print('\n' * 100)
        input("Press enter to exit.")
        print('\n' * 100)
        quit()
    elif yesorno == "yes":
        print('\n' * 100)
        print("Restarting...")
        sleep(2)
        print('\n' * 100)
    else:
        print("Not a valid answer.")